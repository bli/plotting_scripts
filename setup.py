# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages
#from Cython.Build import cythonize

name = "plotting_scripts"

__version__ = "0.2.1"


setup(
    name=name,
    version=__version__,
    description="Some scripts used in Germano Cecere's laboratory at Institut Pasteur.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="GNU GPLv3",
    packages=find_packages(),
    scripts=[
        "scripts/create_metagene_profile.py",
        "scripts/plot_scatterplot.py"],
    install_requires=[
        "cytoolz",
        "deeptools @ git+https://github.com/blaiseli/deepTools.git@issue-1041",
        "gffutils",
        "libhts @ git+https://gitlab.pasteur.fr/bli/libhts.git@36d33ac2c366c0f0702ab50fdcab37261add7b9c",
        "libworkflows @ git+https://gitlab.pasteur.fr/bli/libworkflows.git@b29b854ff1db6c87386007808286207b8af11b9d",
        "matplotlib",
        "pandas"],
    )
