cytoolz
deeptools @ git+https://github.com/blaiseli/deepTools.git@issue-1041
gffutils
libhts @ git+https://gitlab.pasteur.fr/bli/libhts.git@36d33ac2c366c0f0702ab50fdcab37261add7b9c
libworkflows @ git+https://gitlab.pasteur.fr/bli/libworkflows.git@b29b854ff1db6c87386007808286207b8af11b9d
#libhts @ git+https://gitlab.pasteur.fr/bli/libhts.git
#libworkflows @ git+https://gitlab.pasteur.fr/bli/libworkflows.git
matplotlib
pandas
