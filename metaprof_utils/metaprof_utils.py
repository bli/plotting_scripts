# Copyright (C) 2020-2021 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Module containing metaprofile creation utilities based on deeptools.
"""
from copy import deepcopy
from deeptools import heatmapper
from deeptools.plotProfile import Profile
from cytoolz import keyfilter


DEFAULT_PARAMETERS = {
    "upstream": 0,
    "downstream": 0,
    # TODO: change to 2000 ?
    "body": 500,
    "bin size": 10,
    "ref point": None,
    "verbose": False,
    "bin avg type": "mean",
    "missing data as zero": False,
    "min threshold": None,
    "max threshold": None,
    "scale": 1,
    "skip zeros": False,
    "nan after end": False,
    "proc number": 4,
    "sort regions": "keep",
    "sort using": "mean",
    "unscaled 5 prime": 0,
    "unscaled 3 prime": 0,
    "start_label": "start",
    "end_label": "end",
    "label_rotation": 90,
}


def is_prof_param(key):
    """Determine if *key* corresponds to a valid parameter for a *Profile*."""
    return key in {
        "plot_title", "y_axis_label", "y_min", "y_max", "averagetype",
        "reference_point_label", "start_label", "end_label",
        "plot_height", "plot_width", "per_group",
        "plot_type", "image_format", "color_list",
        "legend_location", "plots_per_row", "label_rotation", "dpi"}


def compute_matrix(bigwig_filenames,
                   bed_filename,
                   plot_filename=None,
                   **extra_parameters):
    """Combine information from bigwig files *bigwig_filenames* and bed file
    *bed_filename*.

    If *plot_filename* is set, write the corresponding meta profile
    in this file.
    """
    parameters = deepcopy(DEFAULT_PARAMETERS)
    parameters.update(extra_parameters)
    heatm = heatmapper.heatmapper()
    heatm.computeMatrix(bigwig_filenames, bed_filename, parameters)
    if "sample_labels" in parameters:
        heatm.matrix.set_sample_labels(parameters["sample_labels"])
    if "group_labels" in parameters:
        heatm.matrix.set_group_labels(parameters["group_labels"])
    # Fixing parameters (as in heatmapper.read_matrix_file
    # and heatmapper.save_matrix)
    nb_samples = len(heatm.matrix.sample_labels)
    hm_params = dict()
    for (key, val) in heatm.parameters.items():
        if isinstance(val, list) and not val:
            val = None
        if key in heatm.special_params and not isinstance(val, list):
            val = [val] * nb_samples
            if not val:
                val = [None] * nb_samples
        hm_params[key] = val
    heatm.parameters = hm_params

    if plot_filename is not None:
        print(f"plotting profile to {plot_filename}")
        prof_params = keyfilter(is_prof_param, parameters)
        prof_params["per_group"] = True
        prof = Profile(
            heatm, plot_filename,
            **prof_params)
        prof.plot_profile()
