# Scripts to make plots based on results form high throughput sequencing data

This package contains scripts that are used in [Germano Cecere's
laboratory](https://www.cecerelab.com/) at Institut Pasteur.


## Citing

If you use this package, please cite the following paper:

> Barucci et al, 2020 (doi: [10.1038/s41556-020-0462-7](https://doi.org/10.1038/s41556-020-0462-7))
